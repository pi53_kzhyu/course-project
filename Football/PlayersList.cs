﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Football
{
   public class PlayersList
    {
       public Players[] players;

        public Players[] GenerationPlayersList(int _lenght, string[,] playersArray)
        {
            int lenght = _lenght;
            players = new Players[lenght];
            for (int index = 0; index < lenght; index++)
                players[index] = new Players(Convert.ToUInt32(playersArray[index, 0]), playersArray[index, 1], playersArray[index, 2], DateTime.Parse(playersArray[index, 3]), Convert.ToUInt32(playersArray[index, 4]), Convert.ToUInt32(playersArray[index, 5]));
            return players;
        }
        public Players[] GetPlayersList()
        {
            return players;
        }
    }
}
