﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Football
{
  public  class WorkFile
    {
        
        public List<string> OpenFile(string path)
        {
            string line;
            List<string> lines = new List<string>();
            StreamReader file;
            file = new StreamReader(path);
            while ((line = file.ReadLine()) != null)
            {
                lines.Add(line);
            }
            file.Close();
            return lines;
        }
        public string[,] getFileArray(string path)
        {
            List<string> file = OpenFile(path);
            string[,] array = new string[file.Count, file[0].Split(' ').Length];
            string[] line;
            for (int i = 0; i < file.Count; i++)
            {
                line = file[i].Split(' ');
                for (int j = 0; j < line.Length; j++)                
                    array[i, j] = line[j];
                
            }
            return array;
        }
        public void WriteFileData(string path, List<string> data)
        {
            File.WriteAllLines(path, data);
        }
    }
}
