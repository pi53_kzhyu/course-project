﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Football
{
   public class Team
    {
        protected uint Id;
        protected string NameTeam;
        protected string FootballClub;
        protected string Country;
        protected string []IdsMatchs;
     
        public Team(uint id,string nameTeam,string footballClub,string country, string idsMatch)
        {
            Id = id;
            NameTeam = nameTeam;
            FootballClub = footballClub;
            Country = country;
            IdsMatchs = idsMatch.Split(',');
        }
        public void SetId(uint id)
        {
            if (id > 0)
                Id = id;
        }
        public void SetNameTeam(string nameTeam)
        {
            if (nameTeam.Length > 0)
                NameTeam = nameTeam;
        }
        public void SetFootballClub(string footballClub)
        {
            if (footballClub.Length > 0)
                FootballClub = footballClub;
        }
        public void SetCountry(string country)
        {
            if (country.Length > 0)
                Country = country;
        }
        public void SetIdMatch(string idsMatch)
        {
            IdsMatchs = idsMatch.Split(',');
        }
      
        public uint GetId()
        {
            return Id;
        }
        public string GetNameTeam()
        {
            return NameTeam;
        }
        public string GetFootballClub()
        {
            return FootballClub;
        }
        public string GetCountry()
        {
            return Country;
        }
        public string []GetIdsMatchs()
        {
            return IdsMatchs;
        }
      


    }
}
