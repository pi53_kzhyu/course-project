﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Football
{
    public class Players
    {
        protected string Surname;
        public string Name;
        protected DateTime  Birthday;
        protected uint Team;
        protected uint CountMatch;
        public uint Id;
        public Players(uint id,string name, string surname,DateTime birthday, uint team, uint countMatch)
        {
            Id = id;
            Surname = surname;
            Name = name;
            Birthday = birthday;
            Team = team;
            CountMatch = countMatch;
        }
        public void SetId (uint id)
        {
            if (id > 0)
                Id = id;
        }
        public void SetSurname(string surname)
        {
            if (surname.Length > 0)
                Surname = surname;

        }
        public void SetName(string name)
        {
            if (name.Length > 0)
                Name =name;

        }
        public void SetBirthday(DateTime birthday)
        {
            Birthday = birthday;

        }
        public void SetTeam(uint team)
        {
            if (team > 0)
                Team = team;
        }
        public void SetCountMatch(uint countMatch)
        {
            CountMatch = countMatch;
        }
        public uint GetId()
        {
            return Id;
        }
        public string GetSurname()
        {
            return Surname;
        }
        public string GetName()
        {
            return Name;
        }
        public DateTime GetBirthday()
        {
            return Birthday;
        }
        public uint GetTeam()
        {
            return Team;
        }
        public uint GetCountMatch()
        {
            return CountMatch;
        }
    }
}
