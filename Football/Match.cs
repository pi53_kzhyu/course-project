﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Football
{
    public class Match
    {
        protected string EventName;
        protected string Location;
        protected string Score;
        protected uint Id;
        public Match (uint id, string eventName,string location, string score)
        {
            Id = id;
            EventName = eventName;
            Location = location;
            Score = score;
        }
        public void SetId(uint id)
        {
            if (id > 0)
                Id = id;
        }
        public void SetEventName(string eventName)
        {
            if (eventName.Length > 0)
                EventName = eventName;
        }
        public void SetLocation(string location)
        {
            if (location.Length > 0)
                Location = location;
        }
        public void SetScore(string score)
        {
            if (score.Length>0)
                Score = score;
        }
        public uint GetId()
        {
            return Id;
        }
        public string GetEventName()
        {
            return EventName;
        }
        public string GetLocation()
        {
            return Location;
        }
        public string GetScore()
        {
            return Score;
        }
       
}
}
