﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Football
{
   public class Accounts
    {
        protected string Login;
        protected string Password;
        protected string Access;
        public Accounts (string login, string password, string access)
        {
            Login = login;
            Password = password;
            Access = access;
        }
        public void SetLogin(string login)
        {
            if (login.Length > 0)
                Login = login;
        }
        public void SetPassword(string password)
        {
            if (password.Length > 0)
                Password = password;
        }
        public void SetAccess(string access)
        {
            if (access.Length > 0)
                Access = access;
        }
        public string GetLogin()
        {
            return Login;
        }
        public string GetPassword()
        {
            return Password;
        }
        public string GetAccess()
        {
            return Access;
        }

    }
}
