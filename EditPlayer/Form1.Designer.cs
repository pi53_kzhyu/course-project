﻿namespace EditPlayer
{
    partial class EditPlayers
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.EditName = new System.Windows.Forms.TextBox();
            this.EditSurname = new System.Windows.Forms.TextBox();
            this.EditBirthday = new System.Windows.Forms.DateTimePicker();
            this.EditTeam = new System.Windows.Forms.ComboBox();
            this.EditCount = new System.Windows.Forms.NumericUpDown();
            this.labelEditName = new System.Windows.Forms.Label();
            this.labelEditSurname = new System.Windows.Forms.Label();
            this.labelEditBirthday = new System.Windows.Forms.Label();
            this.labelEditTeam = new System.Windows.Forms.Label();
            this.labelEditCountMatch = new System.Windows.Forms.Label();
            this.buttonEditSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.EditCount)).BeginInit();
            this.SuspendLayout();
            // 
            // EditName
            // 
            this.EditName.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EditName.Location = new System.Drawing.Point(132, 22);
            this.EditName.Name = "EditName";
            this.EditName.Size = new System.Drawing.Size(105, 26);
            this.EditName.TabIndex = 0;
            this.EditName.TextChanged += new System.EventHandler(this.EditName_TextChanged);
            // 
            // EditSurname
            // 
            this.EditSurname.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EditSurname.Location = new System.Drawing.Point(132, 57);
            this.EditSurname.Name = "EditSurname";
            this.EditSurname.Size = new System.Drawing.Size(105, 26);
            this.EditSurname.TabIndex = 1;
            // 
            // EditBirthday
            // 
            this.EditBirthday.CalendarFont = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EditBirthday.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EditBirthday.Location = new System.Drawing.Point(132, 96);
            this.EditBirthday.Name = "EditBirthday";
            this.EditBirthday.Size = new System.Drawing.Size(105, 26);
            this.EditBirthday.TabIndex = 2;
            // 
            // EditTeam
            // 
            this.EditTeam.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EditTeam.FormattingEnabled = true;
            this.EditTeam.Location = new System.Drawing.Point(132, 143);
            this.EditTeam.Name = "EditTeam";
            this.EditTeam.Size = new System.Drawing.Size(105, 27);
            this.EditTeam.TabIndex = 3;
            // 
            // EditCount
            // 
            this.EditCount.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EditCount.Location = new System.Drawing.Point(132, 195);
            this.EditCount.Name = "EditCount";
            this.EditCount.Size = new System.Drawing.Size(105, 26);
            this.EditCount.TabIndex = 4;
            // 
            // labelEditName
            // 
            this.labelEditName.AutoSize = true;
            this.labelEditName.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelEditName.Location = new System.Drawing.Point(13, 25);
            this.labelEditName.Name = "labelEditName";
            this.labelEditName.Size = new System.Drawing.Size(46, 19);
            this.labelEditName.TabIndex = 5;
            this.labelEditName.Text = "Name";
            // 
            // labelEditSurname
            // 
            this.labelEditSurname.AutoSize = true;
            this.labelEditSurname.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelEditSurname.Location = new System.Drawing.Point(11, 60);
            this.labelEditSurname.Name = "labelEditSurname";
            this.labelEditSurname.Size = new System.Drawing.Size(62, 19);
            this.labelEditSurname.TabIndex = 6;
            this.labelEditSurname.Text = "Surname";
            // 
            // labelEditBirthday
            // 
            this.labelEditBirthday.AutoSize = true;
            this.labelEditBirthday.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelEditBirthday.Location = new System.Drawing.Point(12, 102);
            this.labelEditBirthday.Name = "labelEditBirthday";
            this.labelEditBirthday.Size = new System.Drawing.Size(60, 19);
            this.labelEditBirthday.TabIndex = 7;
            this.labelEditBirthday.Text = "Birthday";
            // 
            // labelEditTeam
            // 
            this.labelEditTeam.AutoSize = true;
            this.labelEditTeam.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelEditTeam.Location = new System.Drawing.Point(14, 146);
            this.labelEditTeam.Name = "labelEditTeam";
            this.labelEditTeam.Size = new System.Drawing.Size(42, 19);
            this.labelEditTeam.TabIndex = 8;
            this.labelEditTeam.Text = "Team";
            // 
            // labelEditCountMatch
            // 
            this.labelEditCountMatch.AutoSize = true;
            this.labelEditCountMatch.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelEditCountMatch.Location = new System.Drawing.Point(11, 197);
            this.labelEditCountMatch.Name = "labelEditCountMatch";
            this.labelEditCountMatch.Size = new System.Drawing.Size(89, 19);
            this.labelEditCountMatch.TabIndex = 9;
            this.labelEditCountMatch.Text = "Count Match";
            // 
            // buttonEditSave
            // 
            this.buttonEditSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEditSave.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonEditSave.Location = new System.Drawing.Point(60, 241);
            this.buttonEditSave.Name = "buttonEditSave";
            this.buttonEditSave.Size = new System.Drawing.Size(145, 37);
            this.buttonEditSave.TabIndex = 10;
            this.buttonEditSave.Text = "Save";
            this.buttonEditSave.UseVisualStyleBackColor = true;
            this.buttonEditSave.Click += new System.EventHandler(this.buttonEditSave_Click);
            // 
            // EditPlayers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 290);
            this.Controls.Add(this.buttonEditSave);
            this.Controls.Add(this.labelEditCountMatch);
            this.Controls.Add(this.labelEditTeam);
            this.Controls.Add(this.labelEditBirthday);
            this.Controls.Add(this.labelEditSurname);
            this.Controls.Add(this.labelEditName);
            this.Controls.Add(this.EditCount);
            this.Controls.Add(this.EditTeam);
            this.Controls.Add(this.EditBirthday);
            this.Controls.Add(this.EditSurname);
            this.Controls.Add(this.EditName);
            this.Name = "EditPlayers";
            this.Text = "Edit Players";
            this.Load += new System.EventHandler(this.EditPlayers_Load);
            ((System.ComponentModel.ISupportInitialize)(this.EditCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox EditName;
        private System.Windows.Forms.TextBox EditSurname;
        private System.Windows.Forms.DateTimePicker EditBirthday;
        private System.Windows.Forms.ComboBox EditTeam;
        private System.Windows.Forms.NumericUpDown EditCount;
        private System.Windows.Forms.Label labelEditName;
        private System.Windows.Forms.Label labelEditSurname;
        private System.Windows.Forms.Label labelEditBirthday;
        private System.Windows.Forms.Label labelEditTeam;
        private System.Windows.Forms.Label labelEditCountMatch;
        private System.Windows.Forms.Button buttonEditSave;
    }
}

