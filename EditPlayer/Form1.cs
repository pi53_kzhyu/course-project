﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Football;
using System.IO;

namespace EditPlayer
{
    public partial class EditPlayers : Form
    {
        public Players player;
        public EditPlayers()
        {
            InitializeComponent();
        }
        public EditPlayers(Players _player)
        {
            player = _player;
            InitializeComponent();

        }

        private void EditPlayers_Load(object sender, EventArgs e)
        {  
            
            EditName.Text = player.GetName();
            EditSurname.Text = player.GetSurname();
            EditBirthday.Text = player.GetBirthday().ToString();
           // EditTeam.Text = player.GetTeam();
            EditCount.Text = player.GetCountMatch().ToString();
        }

        private void EditName_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonEditSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog sv = new SaveFileDialog();
            sv.Filter = "Текстовый документ (*.txt)|*.txt|Все файлы (*.*)|*.*";
            if (sv.ShowDialog() == DialogResult.OK)
            {
                StreamWriter streamWriter = new StreamWriter(sv.FileName);
                
                streamWriter.WriteLine(EditName.Text);
                streamWriter.WriteLine(EditSurname.Text);
                streamWriter.WriteLine(EditBirthday.Text);
                streamWriter.WriteLine(EditTeam.Text);
                streamWriter.WriteLine(EditCount.Text);
                streamWriter.Close();
            }
        }
    }
}
