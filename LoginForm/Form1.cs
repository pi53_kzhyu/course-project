﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Football;

namespace LoginFormNamespace
{
    public partial class LoginForm : Form
    {
        Accounts[] accounts;
        bool isAdmin = false;
        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            WorkFile workFile = new WorkFile();
            List<string> accountsFile = workFile.OpenFile(@"D:\Курсова робота\Users.txt");
            int index = 0;
            accounts = new Accounts[accountsFile.Count];
            foreach (string account in accountsFile)
            {
                string[] array;
                array = account.Split(' ');

                accounts[index] = new Accounts(array[0], array[1], array[2]);
                index++;

            }

        }

        private void Login_Click(object sender, EventArgs e)
        {
            string UserLogin, UserPassword;
            
            UserLogin = textBoxName.Text;
            UserPassword = textBoxPassword.Text;
            foreach(Accounts account in accounts)
            {
                if(UserLogin==account.GetLogin()&&UserPassword==account.GetPassword())
                {
                    if ("admin" == account.GetAccess())
                        isAdmin = true;

                }
            }
        }
    }
}
