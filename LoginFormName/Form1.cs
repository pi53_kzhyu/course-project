﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Football;
using InformationsSystem;


namespace LoginFormName
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }
         
        Accounts[] accounts;
        public bool isAdmin = false;
        private void Login_Click(object sender, EventArgs e)
        {
            bool wrongLogin = true;
            string UserLogin, UserPassword;
            UserLogin = textBoxLogin.Text;
            UserPassword = textBoxPassword.Text;            
            foreach (Accounts account in accounts)
            {
                if (UserLogin == account.GetLogin() && UserPassword == account.GetPassword())
                {
                    if ("admin" == account.GetAccess())
                    {
                        isAdmin = true;
                    }
                    wrongLogin = false;
                    this.Hide();
                    CourseWork courseWork = new CourseWork(isAdmin);
                    courseWork.Show();
                    break;
                }
            }
            if(wrongLogin) {
                MessageBox.Show("Wrong login or password!!!");
            }

        }

        private void FormLogin_Load(object sender, EventArgs e)
        {
            WorkFile workFile = new WorkFile();
            List<string> accountsFile = workFile.OpenFile(@"D:\Курсова робота\Users.txt");
            int index = 0;
            accounts = new Accounts[accountsFile.Count];
            foreach (string account in accountsFile)
            {
                string[] array;
                array = account.Split(' ');

                accounts[index] = new Accounts(array[0], array[1], array[2]);
                index++;

            }
        }

        private void textBoxLogin_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelPassword_Click(object sender, EventArgs e)
        {

        }
    }
}
