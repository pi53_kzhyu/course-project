﻿namespace InformationsSystem
{
          
partial class CourseWork
    {
       
       
      
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CourseWork));
            this.Players = new System.Windows.Forms.TabPage();
            this.groupBoxAdminPlayer = new System.Windows.Forms.GroupBox();
            this.AddPlayers = new System.Windows.Forms.GroupBox();
            this.labelAddCountMatch = new System.Windows.Forms.Label();
            this.buttonAddPlayers = new System.Windows.Forms.Button();
            this.labelAddTeam = new System.Windows.Forms.Label();
            this.labelAddBirthday = new System.Windows.Forms.Label();
            this.labelAddSurname = new System.Windows.Forms.Label();
            this.labelAddName = new System.Windows.Forms.Label();
            this.AddCountMatch = new System.Windows.Forms.NumericUpDown();
            this.AddTeam = new System.Windows.Forms.ComboBox();
            this.AddBirthday = new System.Windows.Forms.DateTimePicker();
            this.AddSurname = new System.Windows.Forms.TextBox();
            this.AddName = new System.Windows.Forms.TextBox();
            this.AddPlayer = new System.Windows.Forms.Button();
            this.groupBoxPlayer = new System.Windows.Forms.GroupBox();
            this.buttonEditSave = new System.Windows.Forms.Button();
            this.labelEditCountMatch = new System.Windows.Forms.Label();
            this.labelEditTeam = new System.Windows.Forms.Label();
            this.labelEditBirthday = new System.Windows.Forms.Label();
            this.labelEditSurname = new System.Windows.Forms.Label();
            this.labelEditName = new System.Windows.Forms.Label();
            this.EditCount = new System.Windows.Forms.NumericUpDown();
            this.EditTeam = new System.Windows.Forms.ComboBox();
            this.EditBirthday = new System.Windows.Forms.DateTimePicker();
            this.EditSurname = new System.Windows.Forms.TextBox();
            this.EditName = new System.Windows.Forms.TextBox();
            this.RemovePlayer = new System.Windows.Forms.Button();
            this.listBoxPlayers = new System.Windows.Forms.ListBox();
            this.SearchPlayers = new System.Windows.Forms.TextBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.PlayersList = new System.Windows.Forms.DataGridView();
            this.NamePlayer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Surname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Birthday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameTeam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CountMatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Match = new System.Windows.Forms.TabPage();
            this.groupBoxAdminMatch = new System.Windows.Forms.GroupBox();
            this.groupBoxSelectedMatch = new System.Windows.Forms.GroupBox();
            this.textBox_Score = new System.Windows.Forms.TextBox();
            this.buttonSaveMatch = new System.Windows.Forms.Button();
            this.label_Score = new System.Windows.Forms.Label();
            this.label_Location = new System.Windows.Forms.Label();
            this.labelName_Event = new System.Windows.Forms.Label();
            this.textBox_Location = new System.Windows.Forms.TextBox();
            this.textBox_NameEvent = new System.Windows.Forms.TextBox();
            this.buttonAdd_Match = new System.Windows.Forms.Button();
            this.buttonMatchDelete = new System.Windows.Forms.Button();
            this.AddMatch = new System.Windows.Forms.GroupBox();
            this.textScore = new System.Windows.Forms.TextBox();
            this.Add_Match = new System.Windows.Forms.Button();
            this.labelScore = new System.Windows.Forms.Label();
            this.labelLocation = new System.Windows.Forms.Label();
            this.labelNameEvent = new System.Windows.Forms.Label();
            this.textLocation = new System.Windows.Forms.TextBox();
            this.textNameEvent = new System.Windows.Forms.TextBox();
            this.listBoxMatch = new System.Windows.Forms.ListBox();
            this.SearchMatch = new System.Windows.Forms.TextBox();
            this.buttonSearchMatch = new System.Windows.Forms.Button();
            this.MatchList = new System.Windows.Forms.DataGridView();
            this.NameEvent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Location = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Team = new System.Windows.Forms.TabPage();
            this.groupBoxTeams = new System.Windows.Forms.GroupBox();
            this.groupBoxAddTeam = new System.Windows.Forms.GroupBox();
            this.comboBoxMatch = new System.Windows.Forms.ListBox();
            this.textBoxCountry = new System.Windows.Forms.TextBox();
            this.buttonAddTeam = new System.Windows.Forms.Button();
            this.labelMatch = new System.Windows.Forms.Label();
            this.labelCountry = new System.Windows.Forms.Label();
            this.labelFootballClub = new System.Windows.Forms.Label();
            this.labelNameTeam = new System.Windows.Forms.Label();
            this.textBoxFootballClub = new System.Windows.Forms.TextBox();
            this.textBoxNameTeam = new System.Windows.Forms.TextBox();
            this.buttonNewTeam = new System.Windows.Forms.Button();
            this.groupBoxEditTeam = new System.Windows.Forms.GroupBox();
            this.EdMatch = new System.Windows.Forms.ListBox();
            this.EdCountry = new System.Windows.Forms.TextBox();
            this.buttonSaveTeam = new System.Windows.Forms.Button();
            this.label_Match = new System.Windows.Forms.Label();
            this.label_Country = new System.Windows.Forms.Label();
            this.labelFootball_club = new System.Windows.Forms.Label();
            this.labelName_team = new System.Windows.Forms.Label();
            this.EdFootballClub = new System.Windows.Forms.TextBox();
            this.EdNameTeam = new System.Windows.Forms.TextBox();
            this.buttonRemoveTeam = new System.Windows.Forms.Button();
            this.listBoxTeam = new System.Windows.Forms.ListBox();
            this.SearchTeam = new System.Windows.Forms.TextBox();
            this.buttonSearchTeam = new System.Windows.Forms.Button();
            this.TeamList = new System.Windows.Forms.DataGridView();
            this.TeamName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClubFootball = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdMatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Informations = new System.Windows.Forms.TabControl();
            this.Players.SuspendLayout();
            this.groupBoxAdminPlayer.SuspendLayout();
            this.AddPlayers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddCountMatch)).BeginInit();
            this.groupBoxPlayer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EditCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayersList)).BeginInit();
            this.Match.SuspendLayout();
            this.groupBoxAdminMatch.SuspendLayout();
            this.groupBoxSelectedMatch.SuspendLayout();
            this.AddMatch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MatchList)).BeginInit();
            this.Team.SuspendLayout();
            this.groupBoxTeams.SuspendLayout();
            this.groupBoxAddTeam.SuspendLayout();
            this.groupBoxEditTeam.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TeamList)).BeginInit();
            this.Informations.SuspendLayout();
            this.SuspendLayout();
            // 
            // Players
            // 
            this.Players.BackColor = System.Drawing.Color.AliceBlue;
            this.Players.BackgroundImage = global::InformationsSystem.Properties.Resources._1497784516_d091d0b5d0b7_d0bdd0b0d0b7d0b2d0b0d0bdd0b8d18f;
            this.Players.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Players.Controls.Add(this.groupBoxAdminPlayer);
            this.Players.Controls.Add(this.SearchPlayers);
            this.Players.Controls.Add(this.buttonSearch);
            this.Players.Controls.Add(this.PlayersList);
            this.Players.Location = new System.Drawing.Point(4, 29);
            this.Players.Name = "Players";
            this.Players.Size = new System.Drawing.Size(937, 496);
            this.Players.TabIndex = 0;
            this.Players.Text = "Players";
            // 
            // groupBoxAdminPlayer
            // 
            this.groupBoxAdminPlayer.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxAdminPlayer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBoxAdminPlayer.Controls.Add(this.AddPlayers);
            this.groupBoxAdminPlayer.Controls.Add(this.AddPlayer);
            this.groupBoxAdminPlayer.Controls.Add(this.groupBoxPlayer);
            this.groupBoxAdminPlayer.Controls.Add(this.RemovePlayer);
            this.groupBoxAdminPlayer.Controls.Add(this.listBoxPlayers);
            this.groupBoxAdminPlayer.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxAdminPlayer.Location = new System.Drawing.Point(-4, 0);
            this.groupBoxAdminPlayer.Name = "groupBoxAdminPlayer";
            this.groupBoxAdminPlayer.Size = new System.Drawing.Size(941, 492);
            this.groupBoxAdminPlayer.TabIndex = 1;
            this.groupBoxAdminPlayer.TabStop = false;
            this.groupBoxAdminPlayer.Text = "AdminPanel";
            this.groupBoxAdminPlayer.Visible = false;
            // 
            // AddPlayers
            // 
            this.AddPlayers.BackColor = System.Drawing.Color.FloralWhite;
            this.AddPlayers.Controls.Add(this.labelAddCountMatch);
            this.AddPlayers.Controls.Add(this.buttonAddPlayers);
            this.AddPlayers.Controls.Add(this.labelAddTeam);
            this.AddPlayers.Controls.Add(this.labelAddBirthday);
            this.AddPlayers.Controls.Add(this.labelAddSurname);
            this.AddPlayers.Controls.Add(this.labelAddName);
            this.AddPlayers.Controls.Add(this.AddCountMatch);
            this.AddPlayers.Controls.Add(this.AddTeam);
            this.AddPlayers.Controls.Add(this.AddBirthday);
            this.AddPlayers.Controls.Add(this.AddSurname);
            this.AddPlayers.Controls.Add(this.AddName);
            this.AddPlayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddPlayers.Location = new System.Drawing.Point(607, 85);
            this.AddPlayers.Name = "AddPlayers";
            this.AddPlayers.Size = new System.Drawing.Size(278, 327);
            this.AddPlayers.TabIndex = 5;
            this.AddPlayers.TabStop = false;
            this.AddPlayers.Text = "Add Player";
            this.AddPlayers.Enter += new System.EventHandler(this.AddPlayers_Enter);
            // 
            // labelAddCountMatch
            // 
            this.labelAddCountMatch.AutoSize = true;
            this.labelAddCountMatch.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAddCountMatch.ForeColor = System.Drawing.Color.Navy;
            this.labelAddCountMatch.Location = new System.Drawing.Point(31, 194);
            this.labelAddCountMatch.Name = "labelAddCountMatch";
            this.labelAddCountMatch.Size = new System.Drawing.Size(115, 22);
            this.labelAddCountMatch.TabIndex = 20;
            this.labelAddCountMatch.Text = "Count Match";
            // 
            // buttonAddPlayers
            // 
            this.buttonAddPlayers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddPlayers.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAddPlayers.ForeColor = System.Drawing.Color.Navy;
            this.buttonAddPlayers.Location = new System.Drawing.Point(35, 248);
            this.buttonAddPlayers.Name = "buttonAddPlayers";
            this.buttonAddPlayers.Size = new System.Drawing.Size(219, 43);
            this.buttonAddPlayers.TabIndex = 21;
            this.buttonAddPlayers.Text = "Add";
            this.buttonAddPlayers.UseVisualStyleBackColor = true;
            this.buttonAddPlayers.Click += new System.EventHandler(this.buttonAddPlayers_Click);
            // 
            // labelAddTeam
            // 
            this.labelAddTeam.AutoSize = true;
            this.labelAddTeam.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAddTeam.ForeColor = System.Drawing.Color.Navy;
            this.labelAddTeam.Location = new System.Drawing.Point(34, 143);
            this.labelAddTeam.Name = "labelAddTeam";
            this.labelAddTeam.Size = new System.Drawing.Size(57, 22);
            this.labelAddTeam.TabIndex = 19;
            this.labelAddTeam.Text = "Team";
            // 
            // labelAddBirthday
            // 
            this.labelAddBirthday.AutoSize = true;
            this.labelAddBirthday.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAddBirthday.ForeColor = System.Drawing.Color.Navy;
            this.labelAddBirthday.Location = new System.Drawing.Point(32, 99);
            this.labelAddBirthday.Name = "labelAddBirthday";
            this.labelAddBirthday.Size = new System.Drawing.Size(78, 22);
            this.labelAddBirthday.TabIndex = 18;
            this.labelAddBirthday.Text = "Birthday";
            // 
            // labelAddSurname
            // 
            this.labelAddSurname.AutoSize = true;
            this.labelAddSurname.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAddSurname.ForeColor = System.Drawing.Color.Navy;
            this.labelAddSurname.Location = new System.Drawing.Point(31, 57);
            this.labelAddSurname.Name = "labelAddSurname";
            this.labelAddSurname.Size = new System.Drawing.Size(86, 22);
            this.labelAddSurname.TabIndex = 17;
            this.labelAddSurname.Text = "Surname";
            // 
            // labelAddName
            // 
            this.labelAddName.AutoSize = true;
            this.labelAddName.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAddName.ForeColor = System.Drawing.Color.Navy;
            this.labelAddName.Location = new System.Drawing.Point(33, 22);
            this.labelAddName.Name = "labelAddName";
            this.labelAddName.Size = new System.Drawing.Size(60, 22);
            this.labelAddName.TabIndex = 16;
            this.labelAddName.Text = "Name";
            // 
            // AddCountMatch
            // 
            this.AddCountMatch.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddCountMatch.Location = new System.Drawing.Point(152, 192);
            this.AddCountMatch.Name = "AddCountMatch";
            this.AddCountMatch.Size = new System.Drawing.Size(120, 26);
            this.AddCountMatch.TabIndex = 15;
            // 
            // AddTeam
            // 
            this.AddTeam.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddTeam.FormattingEnabled = true;
            this.AddTeam.Location = new System.Drawing.Point(152, 140);
            this.AddTeam.Name = "AddTeam";
            this.AddTeam.Size = new System.Drawing.Size(120, 27);
            this.AddTeam.TabIndex = 14;
            // 
            // AddBirthday
            // 
            this.AddBirthday.CalendarFont = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddBirthday.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddBirthday.Location = new System.Drawing.Point(152, 93);
            this.AddBirthday.Name = "AddBirthday";
            this.AddBirthday.Size = new System.Drawing.Size(120, 29);
            this.AddBirthday.TabIndex = 13;
            // 
            // AddSurname
            // 
            this.AddSurname.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddSurname.Location = new System.Drawing.Point(152, 54);
            this.AddSurname.Name = "AddSurname";
            this.AddSurname.Size = new System.Drawing.Size(120, 26);
            this.AddSurname.TabIndex = 12;
            // 
            // AddName
            // 
            this.AddName.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddName.Location = new System.Drawing.Point(152, 19);
            this.AddName.Name = "AddName";
            this.AddName.Size = new System.Drawing.Size(120, 26);
            this.AddName.TabIndex = 11;
            // 
            // AddPlayer
            // 
            this.AddPlayer.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.AddPlayer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddPlayer.ForeColor = System.Drawing.Color.DarkBlue;
            this.AddPlayer.Location = new System.Drawing.Point(658, 39);
            this.AddPlayer.Name = "AddPlayer";
            this.AddPlayer.Size = new System.Drawing.Size(184, 40);
            this.AddPlayer.TabIndex = 4;
            this.AddPlayer.Text = "Create new Player";
            this.AddPlayer.UseVisualStyleBackColor = false;
            this.AddPlayer.Click += new System.EventHandler(this.AddPlayer_Click);
            // 
            // groupBoxPlayer
            // 
            this.groupBoxPlayer.BackColor = System.Drawing.Color.FloralWhite;
            this.groupBoxPlayer.Controls.Add(this.buttonEditSave);
            this.groupBoxPlayer.Controls.Add(this.labelEditCountMatch);
            this.groupBoxPlayer.Controls.Add(this.labelEditTeam);
            this.groupBoxPlayer.Controls.Add(this.labelEditBirthday);
            this.groupBoxPlayer.Controls.Add(this.labelEditSurname);
            this.groupBoxPlayer.Controls.Add(this.labelEditName);
            this.groupBoxPlayer.Controls.Add(this.EditCount);
            this.groupBoxPlayer.Controls.Add(this.EditTeam);
            this.groupBoxPlayer.Controls.Add(this.EditBirthday);
            this.groupBoxPlayer.Controls.Add(this.EditSurname);
            this.groupBoxPlayer.Controls.Add(this.EditName);
            this.groupBoxPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxPlayer.Location = new System.Drawing.Point(298, 85);
            this.groupBoxPlayer.Name = "groupBoxPlayer";
            this.groupBoxPlayer.Size = new System.Drawing.Size(272, 327);
            this.groupBoxPlayer.TabIndex = 3;
            this.groupBoxPlayer.TabStop = false;
            this.groupBoxPlayer.Text = "Selected Player";
            this.groupBoxPlayer.Enter += new System.EventHandler(this.groupBoxPlayer_Enter);
            // 
            // buttonEditSave
            // 
            this.buttonEditSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEditSave.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonEditSave.ForeColor = System.Drawing.Color.Navy;
            this.buttonEditSave.Location = new System.Drawing.Point(23, 248);
            this.buttonEditSave.Name = "buttonEditSave";
            this.buttonEditSave.Size = new System.Drawing.Size(219, 43);
            this.buttonEditSave.TabIndex = 21;
            this.buttonEditSave.Text = "Save";
            this.buttonEditSave.UseVisualStyleBackColor = true;
            this.buttonEditSave.Click += new System.EventHandler(this.buttonEditSave_Click);
            // 
            // labelEditCountMatch
            // 
            this.labelEditCountMatch.AutoSize = true;
            this.labelEditCountMatch.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelEditCountMatch.ForeColor = System.Drawing.Color.Navy;
            this.labelEditCountMatch.Location = new System.Drawing.Point(17, 195);
            this.labelEditCountMatch.Name = "labelEditCountMatch";
            this.labelEditCountMatch.Size = new System.Drawing.Size(115, 22);
            this.labelEditCountMatch.TabIndex = 20;
            this.labelEditCountMatch.Text = "Count Match";
            // 
            // labelEditTeam
            // 
            this.labelEditTeam.AutoSize = true;
            this.labelEditTeam.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelEditTeam.ForeColor = System.Drawing.Color.Navy;
            this.labelEditTeam.Location = new System.Drawing.Point(17, 147);
            this.labelEditTeam.Name = "labelEditTeam";
            this.labelEditTeam.Size = new System.Drawing.Size(57, 22);
            this.labelEditTeam.TabIndex = 19;
            this.labelEditTeam.Text = "Team";
            // 
            // labelEditBirthday
            // 
            this.labelEditBirthday.AutoSize = true;
            this.labelEditBirthday.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelEditBirthday.ForeColor = System.Drawing.Color.Navy;
            this.labelEditBirthday.Location = new System.Drawing.Point(17, 100);
            this.labelEditBirthday.Name = "labelEditBirthday";
            this.labelEditBirthday.Size = new System.Drawing.Size(78, 22);
            this.labelEditBirthday.TabIndex = 18;
            this.labelEditBirthday.Text = "Birthday";
            // 
            // labelEditSurname
            // 
            this.labelEditSurname.AutoSize = true;
            this.labelEditSurname.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelEditSurname.ForeColor = System.Drawing.Color.Navy;
            this.labelEditSurname.Location = new System.Drawing.Point(17, 58);
            this.labelEditSurname.Name = "labelEditSurname";
            this.labelEditSurname.Size = new System.Drawing.Size(86, 22);
            this.labelEditSurname.TabIndex = 17;
            this.labelEditSurname.Text = "Surname";
            // 
            // labelEditName
            // 
            this.labelEditName.AutoSize = true;
            this.labelEditName.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelEditName.ForeColor = System.Drawing.Color.Navy;
            this.labelEditName.Location = new System.Drawing.Point(17, 23);
            this.labelEditName.Name = "labelEditName";
            this.labelEditName.Size = new System.Drawing.Size(60, 22);
            this.labelEditName.TabIndex = 16;
            this.labelEditName.Text = "Name";
            // 
            // EditCount
            // 
            this.EditCount.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EditCount.Location = new System.Drawing.Point(138, 192);
            this.EditCount.Name = "EditCount";
            this.EditCount.Size = new System.Drawing.Size(119, 26);
            this.EditCount.TabIndex = 15;
            // 
            // EditTeam
            // 
            this.EditTeam.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EditTeam.FormattingEnabled = true;
            this.EditTeam.Location = new System.Drawing.Point(138, 140);
            this.EditTeam.Name = "EditTeam";
            this.EditTeam.Size = new System.Drawing.Size(119, 27);
            this.EditTeam.TabIndex = 14;
            // 
            // EditBirthday
            // 
            this.EditBirthday.CalendarFont = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EditBirthday.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EditBirthday.Location = new System.Drawing.Point(138, 93);
            this.EditBirthday.Name = "EditBirthday";
            this.EditBirthday.Size = new System.Drawing.Size(119, 29);
            this.EditBirthday.TabIndex = 13;
            // 
            // EditSurname
            // 
            this.EditSurname.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EditSurname.Location = new System.Drawing.Point(138, 54);
            this.EditSurname.Name = "EditSurname";
            this.EditSurname.Size = new System.Drawing.Size(119, 26);
            this.EditSurname.TabIndex = 12;
            // 
            // EditName
            // 
            this.EditName.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EditName.Location = new System.Drawing.Point(138, 19);
            this.EditName.Name = "EditName";
            this.EditName.Size = new System.Drawing.Size(119, 26);
            this.EditName.TabIndex = 11;
            // 
            // RemovePlayer
            // 
            this.RemovePlayer.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.RemovePlayer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RemovePlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RemovePlayer.ForeColor = System.Drawing.Color.DarkBlue;
            this.RemovePlayer.Location = new System.Drawing.Point(360, 39);
            this.RemovePlayer.Name = "RemovePlayer";
            this.RemovePlayer.Size = new System.Drawing.Size(161, 40);
            this.RemovePlayer.TabIndex = 1;
            this.RemovePlayer.Text = "Remove";
            this.RemovePlayer.UseVisualStyleBackColor = false;
            this.RemovePlayer.Click += new System.EventHandler(this.RemovePlayer_Click);
            this.RemovePlayer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RemovePlayer_MouseClick);
            // 
            // listBoxPlayers
            // 
            this.listBoxPlayers.BackColor = System.Drawing.Color.FloralWhite;
            this.listBoxPlayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBoxPlayers.ForeColor = System.Drawing.Color.MidnightBlue;
            this.listBoxPlayers.FormattingEnabled = true;
            this.listBoxPlayers.ItemHeight = 20;
            this.listBoxPlayers.Location = new System.Drawing.Point(10, 28);
            this.listBoxPlayers.Name = "listBoxPlayers";
            this.listBoxPlayers.Size = new System.Drawing.Size(253, 384);
            this.listBoxPlayers.TabIndex = 0;
            this.listBoxPlayers.SelectedIndexChanged += new System.EventHandler(this.listBoxPlayers_SelectedIndexChanged);
            this.listBoxPlayers.SelectedValueChanged += new System.EventHandler(this.listBoxPlayers_SelectedValueChanged);
            // 
            // SearchPlayers
            // 
            this.SearchPlayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SearchPlayers.Location = new System.Drawing.Point(16, 28);
            this.SearchPlayers.Name = "SearchPlayers";
            this.SearchPlayers.Size = new System.Drawing.Size(314, 29);
            this.SearchPlayers.TabIndex = 2;
            // 
            // buttonSearch
            // 
            this.buttonSearch.BackColor = System.Drawing.Color.Ivory;
            this.buttonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSearch.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonSearch.Location = new System.Drawing.Point(366, 17);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(180, 40);
            this.buttonSearch.TabIndex = 3;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // PlayersList
            // 
            this.PlayersList.BackgroundColor = System.Drawing.Color.LightGoldenrodYellow;
            this.PlayersList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PlayersList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NamePlayer,
            this.Surname,
            this.Birthday,
            this.NameTeam,
            this.CountMatch});
            this.PlayersList.GridColor = System.Drawing.Color.DarkOliveGreen;
            this.PlayersList.Location = new System.Drawing.Point(6, 67);
            this.PlayersList.Name = "PlayersList";
            this.PlayersList.ReadOnly = true;
            this.PlayersList.Size = new System.Drawing.Size(547, 378);
            this.PlayersList.TabIndex = 0;
            // 
            // NamePlayer
            // 
            this.NamePlayer.HeaderText = "Name";
            this.NamePlayer.Name = "NamePlayer";
            this.NamePlayer.ReadOnly = true;
            // 
            // Surname
            // 
            this.Surname.HeaderText = "Surname";
            this.Surname.Name = "Surname";
            this.Surname.ReadOnly = true;
            // 
            // Birthday
            // 
            this.Birthday.HeaderText = "Birthday";
            this.Birthday.Name = "Birthday";
            this.Birthday.ReadOnly = true;
            // 
            // NameTeam
            // 
            this.NameTeam.HeaderText = "NameTeam";
            this.NameTeam.Name = "NameTeam";
            this.NameTeam.ReadOnly = true;
            // 
            // CountMatch
            // 
            this.CountMatch.HeaderText = "Count Match";
            this.CountMatch.Name = "CountMatch";
            this.CountMatch.ReadOnly = true;
            // 
            // Match
            // 
            this.Match.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Match.BackgroundImage")));
            this.Match.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Match.Controls.Add(this.groupBoxAdminMatch);
            this.Match.Controls.Add(this.SearchMatch);
            this.Match.Controls.Add(this.buttonSearchMatch);
            this.Match.Controls.Add(this.MatchList);
            this.Match.Location = new System.Drawing.Point(4, 29);
            this.Match.Name = "Match";
            this.Match.Size = new System.Drawing.Size(937, 492);
            this.Match.TabIndex = 2;
            this.Match.Text = "Match";
            this.Match.UseVisualStyleBackColor = true;
            // 
            // groupBoxAdminMatch
            // 
            this.groupBoxAdminMatch.Controls.Add(this.groupBoxSelectedMatch);
            this.groupBoxAdminMatch.Controls.Add(this.buttonAdd_Match);
            this.groupBoxAdminMatch.Controls.Add(this.buttonMatchDelete);
            this.groupBoxAdminMatch.Controls.Add(this.AddMatch);
            this.groupBoxAdminMatch.Controls.Add(this.listBoxMatch);
            this.groupBoxAdminMatch.Location = new System.Drawing.Point(-4, 7);
            this.groupBoxAdminMatch.Name = "groupBoxAdminMatch";
            this.groupBoxAdminMatch.Size = new System.Drawing.Size(920, 479);
            this.groupBoxAdminMatch.TabIndex = 11;
            this.groupBoxAdminMatch.TabStop = false;
            this.groupBoxAdminMatch.Text = "AdminPanel";
            this.groupBoxAdminMatch.Visible = false;
            // 
            // groupBoxSelectedMatch
            // 
            this.groupBoxSelectedMatch.BackColor = System.Drawing.Color.FloralWhite;
            this.groupBoxSelectedMatch.Controls.Add(this.textBox_Score);
            this.groupBoxSelectedMatch.Controls.Add(this.buttonSaveMatch);
            this.groupBoxSelectedMatch.Controls.Add(this.label_Score);
            this.groupBoxSelectedMatch.Controls.Add(this.label_Location);
            this.groupBoxSelectedMatch.Controls.Add(this.labelName_Event);
            this.groupBoxSelectedMatch.Controls.Add(this.textBox_Location);
            this.groupBoxSelectedMatch.Controls.Add(this.textBox_NameEvent);
            this.groupBoxSelectedMatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxSelectedMatch.Location = new System.Drawing.Point(228, 84);
            this.groupBoxSelectedMatch.Name = "groupBoxSelectedMatch";
            this.groupBoxSelectedMatch.Size = new System.Drawing.Size(318, 227);
            this.groupBoxSelectedMatch.TabIndex = 11;
            this.groupBoxSelectedMatch.TabStop = false;
            this.groupBoxSelectedMatch.Text = "Selected Match";
            // 
            // textBox_Score
            // 
            this.textBox_Score.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_Score.Location = new System.Drawing.Point(152, 99);
            this.textBox_Score.Name = "textBox_Score";
            this.textBox_Score.Size = new System.Drawing.Size(145, 26);
            this.textBox_Score.TabIndex = 22;
            // 
            // buttonSaveMatch
            // 
            this.buttonSaveMatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSaveMatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSaveMatch.ForeColor = System.Drawing.Color.Black;
            this.buttonSaveMatch.Location = new System.Drawing.Point(38, 149);
            this.buttonSaveMatch.Name = "buttonSaveMatch";
            this.buttonSaveMatch.Size = new System.Drawing.Size(219, 43);
            this.buttonSaveMatch.TabIndex = 21;
            this.buttonSaveMatch.Text = "Save";
            this.buttonSaveMatch.UseVisualStyleBackColor = true;
            this.buttonSaveMatch.Click += new System.EventHandler(this.buttonSaveMatch_Click);
            // 
            // label_Score
            // 
            this.label_Score.AutoSize = true;
            this.label_Score.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Score.ForeColor = System.Drawing.Color.DarkBlue;
            this.label_Score.Location = new System.Drawing.Point(32, 99);
            this.label_Score.Name = "label_Score";
            this.label_Score.Size = new System.Drawing.Size(60, 24);
            this.label_Score.TabIndex = 18;
            this.label_Score.Text = "Score";
            // 
            // label_Location
            // 
            this.label_Location.AutoSize = true;
            this.label_Location.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Location.ForeColor = System.Drawing.Color.DarkBlue;
            this.label_Location.Location = new System.Drawing.Point(31, 57);
            this.label_Location.Name = "label_Location";
            this.label_Location.Size = new System.Drawing.Size(81, 24);
            this.label_Location.TabIndex = 17;
            this.label_Location.Text = "Location";
            // 
            // labelName_Event
            // 
            this.labelName_Event.AutoSize = true;
            this.labelName_Event.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName_Event.ForeColor = System.Drawing.Color.DarkBlue;
            this.labelName_Event.Location = new System.Drawing.Point(33, 22);
            this.labelName_Event.Name = "labelName_Event";
            this.labelName_Event.Size = new System.Drawing.Size(114, 24);
            this.labelName_Event.TabIndex = 16;
            this.labelName_Event.Text = "Name Event";
            // 
            // textBox_Location
            // 
            this.textBox_Location.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_Location.Location = new System.Drawing.Point(152, 54);
            this.textBox_Location.Name = "textBox_Location";
            this.textBox_Location.Size = new System.Drawing.Size(145, 26);
            this.textBox_Location.TabIndex = 12;
            // 
            // textBox_NameEvent
            // 
            this.textBox_NameEvent.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_NameEvent.Location = new System.Drawing.Point(152, 19);
            this.textBox_NameEvent.Name = "textBox_NameEvent";
            this.textBox_NameEvent.Size = new System.Drawing.Size(145, 26);
            this.textBox_NameEvent.TabIndex = 11;
            // 
            // buttonAdd_Match
            // 
            this.buttonAdd_Match.BackColor = System.Drawing.Color.White;
            this.buttonAdd_Match.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonAdd_Match.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAdd_Match.ForeColor = System.Drawing.Color.Black;
            this.buttonAdd_Match.Location = new System.Drawing.Point(600, 14);
            this.buttonAdd_Match.Name = "buttonAdd_Match";
            this.buttonAdd_Match.Size = new System.Drawing.Size(234, 47);
            this.buttonAdd_Match.TabIndex = 8;
            this.buttonAdd_Match.Text = "Create new match";
            this.buttonAdd_Match.UseVisualStyleBackColor = false;
            this.buttonAdd_Match.Click += new System.EventHandler(this.buttonAdd_Match_Click);
            // 
            // buttonMatchDelete
            // 
            this.buttonMatchDelete.BackColor = System.Drawing.Color.White;
            this.buttonMatchDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonMatchDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonMatchDelete.ForeColor = System.Drawing.Color.Black;
            this.buttonMatchDelete.Location = new System.Drawing.Point(278, 14);
            this.buttonMatchDelete.Name = "buttonMatchDelete";
            this.buttonMatchDelete.Size = new System.Drawing.Size(189, 47);
            this.buttonMatchDelete.TabIndex = 10;
            this.buttonMatchDelete.Text = "Delete";
            this.buttonMatchDelete.UseVisualStyleBackColor = false;
            this.buttonMatchDelete.Click += new System.EventHandler(this.buttonMatchDelete_Click);
            // 
            // AddMatch
            // 
            this.AddMatch.BackColor = System.Drawing.Color.FloralWhite;
            this.AddMatch.Controls.Add(this.textScore);
            this.AddMatch.Controls.Add(this.Add_Match);
            this.AddMatch.Controls.Add(this.labelScore);
            this.AddMatch.Controls.Add(this.labelLocation);
            this.AddMatch.Controls.Add(this.labelNameEvent);
            this.AddMatch.Controls.Add(this.textLocation);
            this.AddMatch.Controls.Add(this.textNameEvent);
            this.AddMatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddMatch.Location = new System.Drawing.Point(563, 84);
            this.AddMatch.Name = "AddMatch";
            this.AddMatch.Size = new System.Drawing.Size(318, 227);
            this.AddMatch.TabIndex = 9;
            this.AddMatch.TabStop = false;
            this.AddMatch.Text = "Add Match";
            this.AddMatch.Enter += new System.EventHandler(this.AddMatch_Enter);
            // 
            // textScore
            // 
            this.textScore.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textScore.Location = new System.Drawing.Point(152, 99);
            this.textScore.Name = "textScore";
            this.textScore.Size = new System.Drawing.Size(145, 26);
            this.textScore.TabIndex = 22;
            // 
            // Add_Match
            // 
            this.Add_Match.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Add_Match.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Add_Match.ForeColor = System.Drawing.Color.Black;
            this.Add_Match.Location = new System.Drawing.Point(52, 149);
            this.Add_Match.Name = "Add_Match";
            this.Add_Match.Size = new System.Drawing.Size(219, 43);
            this.Add_Match.TabIndex = 21;
            this.Add_Match.Text = "Add Match";
            this.Add_Match.UseVisualStyleBackColor = true;
            this.Add_Match.Click += new System.EventHandler(this.Add_Match_Click);
            // 
            // labelScore
            // 
            this.labelScore.AutoSize = true;
            this.labelScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScore.ForeColor = System.Drawing.Color.DarkBlue;
            this.labelScore.Location = new System.Drawing.Point(32, 99);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(60, 24);
            this.labelScore.TabIndex = 18;
            this.labelScore.Text = "Score";
            // 
            // labelLocation
            // 
            this.labelLocation.AutoSize = true;
            this.labelLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLocation.ForeColor = System.Drawing.Color.DarkBlue;
            this.labelLocation.Location = new System.Drawing.Point(31, 57);
            this.labelLocation.Name = "labelLocation";
            this.labelLocation.Size = new System.Drawing.Size(81, 24);
            this.labelLocation.TabIndex = 17;
            this.labelLocation.Text = "Location";
            // 
            // labelNameEvent
            // 
            this.labelNameEvent.AutoSize = true;
            this.labelNameEvent.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNameEvent.ForeColor = System.Drawing.Color.DarkBlue;
            this.labelNameEvent.Location = new System.Drawing.Point(33, 22);
            this.labelNameEvent.Name = "labelNameEvent";
            this.labelNameEvent.Size = new System.Drawing.Size(114, 24);
            this.labelNameEvent.TabIndex = 16;
            this.labelNameEvent.Text = "Name Event";
            // 
            // textLocation
            // 
            this.textLocation.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textLocation.Location = new System.Drawing.Point(152, 54);
            this.textLocation.Name = "textLocation";
            this.textLocation.Size = new System.Drawing.Size(145, 26);
            this.textLocation.TabIndex = 12;
            // 
            // textNameEvent
            // 
            this.textNameEvent.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textNameEvent.Location = new System.Drawing.Point(152, 19);
            this.textNameEvent.Name = "textNameEvent";
            this.textNameEvent.Size = new System.Drawing.Size(145, 26);
            this.textNameEvent.TabIndex = 11;
            // 
            // listBoxMatch
            // 
            this.listBoxMatch.BackColor = System.Drawing.Color.FloralWhite;
            this.listBoxMatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBoxMatch.ForeColor = System.Drawing.Color.Navy;
            this.listBoxMatch.FormattingEnabled = true;
            this.listBoxMatch.ItemHeight = 20;
            this.listBoxMatch.Location = new System.Drawing.Point(9, 28);
            this.listBoxMatch.Name = "listBoxMatch";
            this.listBoxMatch.Size = new System.Drawing.Size(210, 384);
            this.listBoxMatch.TabIndex = 0;
            this.listBoxMatch.SelectedIndexChanged += new System.EventHandler(this.listBoxMatch_SelectedIndexChanged);
            this.listBoxMatch.SelectedValueChanged += new System.EventHandler(this.listBoxMatch_SelectedValueChanged);
            // 
            // SearchMatch
            // 
            this.SearchMatch.Location = new System.Drawing.Point(25, 21);
            this.SearchMatch.Name = "SearchMatch";
            this.SearchMatch.Size = new System.Drawing.Size(229, 26);
            this.SearchMatch.TabIndex = 6;
            // 
            // buttonSearchMatch
            // 
            this.buttonSearchMatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearchMatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSearchMatch.ForeColor = System.Drawing.Color.Gold;
            this.buttonSearchMatch.Location = new System.Drawing.Point(266, 12);
            this.buttonSearchMatch.Name = "buttonSearchMatch";
            this.buttonSearchMatch.Size = new System.Drawing.Size(146, 48);
            this.buttonSearchMatch.TabIndex = 7;
            this.buttonSearchMatch.Text = "Search";
            this.buttonSearchMatch.UseVisualStyleBackColor = true;
            this.buttonSearchMatch.Click += new System.EventHandler(this.buttonSearchMatch_Click);
            // 
            // MatchList
            // 
            this.MatchList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.MatchList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MatchList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameEvent,
            this.Location,
            this.Score});
            this.MatchList.GridColor = System.Drawing.Color.DarkCyan;
            this.MatchList.Location = new System.Drawing.Point(25, 66);
            this.MatchList.Name = "MatchList";
            this.MatchList.Size = new System.Drawing.Size(387, 423);
            this.MatchList.TabIndex = 0;
            this.MatchList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MatchList_CellContentClick_1);
            // 
            // NameEvent
            // 
            this.NameEvent.HeaderText = "Name Event";
            this.NameEvent.Name = "NameEvent";
            // 
            // Location
            // 
            this.Location.HeaderText = "Location";
            this.Location.Name = "Location";
            // 
            // Score
            // 
            this.Score.HeaderText = "Score";
            this.Score.Name = "Score";
            // 
            // Team
            // 
            this.Team.BackColor = System.Drawing.Color.Transparent;
            this.Team.BackgroundImage = global::InformationsSystem.Properties.Resources._1497784516_d091d0b5d0b7_d0bdd0b0d0b7d0b2d0b0d0bdd0b8d18f;
            this.Team.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Team.Controls.Add(this.groupBoxTeams);
            this.Team.Controls.Add(this.SearchTeam);
            this.Team.Controls.Add(this.buttonSearchTeam);
            this.Team.Controls.Add(this.TeamList);
            this.Team.Location = new System.Drawing.Point(4, 29);
            this.Team.Name = "Team";
            this.Team.Padding = new System.Windows.Forms.Padding(3);
            this.Team.Size = new System.Drawing.Size(937, 492);
            this.Team.TabIndex = 1;
            this.Team.Text = "Team";
            // 
            // groupBoxTeams
            // 
            this.groupBoxTeams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxTeams.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxTeams.Controls.Add(this.groupBoxAddTeam);
            this.groupBoxTeams.Controls.Add(this.buttonNewTeam);
            this.groupBoxTeams.Controls.Add(this.groupBoxEditTeam);
            this.groupBoxTeams.Controls.Add(this.buttonRemoveTeam);
            this.groupBoxTeams.Controls.Add(this.listBoxTeam);
            this.groupBoxTeams.ForeColor = System.Drawing.Color.White;
            this.groupBoxTeams.Location = new System.Drawing.Point(0, 0);
            this.groupBoxTeams.Name = "groupBoxTeams";
            this.groupBoxTeams.Size = new System.Drawing.Size(931, 485);
            this.groupBoxTeams.TabIndex = 6;
            this.groupBoxTeams.TabStop = false;
            this.groupBoxTeams.Text = "AdminPanel";
            this.groupBoxTeams.Visible = false;
            this.groupBoxTeams.Enter += new System.EventHandler(this.groupBoxTeams_Enter);
            // 
            // groupBoxAddTeam
            // 
            this.groupBoxAddTeam.BackColor = System.Drawing.Color.FloralWhite;
            this.groupBoxAddTeam.Controls.Add(this.comboBoxMatch);
            this.groupBoxAddTeam.Controls.Add(this.textBoxCountry);
            this.groupBoxAddTeam.Controls.Add(this.buttonAddTeam);
            this.groupBoxAddTeam.Controls.Add(this.labelMatch);
            this.groupBoxAddTeam.Controls.Add(this.labelCountry);
            this.groupBoxAddTeam.Controls.Add(this.labelFootballClub);
            this.groupBoxAddTeam.Controls.Add(this.labelNameTeam);
            this.groupBoxAddTeam.Controls.Add(this.textBoxFootballClub);
            this.groupBoxAddTeam.Controls.Add(this.textBoxNameTeam);
            this.groupBoxAddTeam.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxAddTeam.Location = new System.Drawing.Point(581, 62);
            this.groupBoxAddTeam.Name = "groupBoxAddTeam";
            this.groupBoxAddTeam.Size = new System.Drawing.Size(266, 350);
            this.groupBoxAddTeam.TabIndex = 5;
            this.groupBoxAddTeam.TabStop = false;
            this.groupBoxAddTeam.Text = "Add Team";
            // 
            // comboBoxMatch
            // 
            this.comboBoxMatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxMatch.ForeColor = System.Drawing.Color.Black;
            this.comboBoxMatch.FormattingEnabled = true;
            this.comboBoxMatch.ItemHeight = 20;
            this.comboBoxMatch.Location = new System.Drawing.Point(72, 154);
            this.comboBoxMatch.Name = "comboBoxMatch";
            this.comboBoxMatch.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.comboBoxMatch.Size = new System.Drawing.Size(185, 124);
            this.comboBoxMatch.TabIndex = 24;
            this.comboBoxMatch.SelectedIndexChanged += new System.EventHandler(this.comboBoxMatch_SelectedIndexChanged);
            // 
            // textBoxCountry
            // 
            this.textBoxCountry.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxCountry.ForeColor = System.Drawing.Color.Black;
            this.textBoxCountry.Location = new System.Drawing.Point(134, 111);
            this.textBoxCountry.Name = "textBoxCountry";
            this.textBoxCountry.Size = new System.Drawing.Size(123, 29);
            this.textBoxCountry.TabIndex = 22;
            // 
            // buttonAddTeam
            // 
            this.buttonAddTeam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddTeam.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAddTeam.ForeColor = System.Drawing.Color.MidnightBlue;
            this.buttonAddTeam.Location = new System.Drawing.Point(35, 289);
            this.buttonAddTeam.Name = "buttonAddTeam";
            this.buttonAddTeam.Size = new System.Drawing.Size(219, 43);
            this.buttonAddTeam.TabIndex = 21;
            this.buttonAddTeam.Text = "Add";
            this.buttonAddTeam.UseVisualStyleBackColor = true;
            this.buttonAddTeam.Click += new System.EventHandler(this.buttonAddTeam_Click_1);
            // 
            // labelMatch
            // 
            this.labelMatch.AutoSize = true;
            this.labelMatch.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMatch.ForeColor = System.Drawing.Color.Navy;
            this.labelMatch.Location = new System.Drawing.Point(6, 154);
            this.labelMatch.Name = "labelMatch";
            this.labelMatch.Size = new System.Drawing.Size(60, 22);
            this.labelMatch.TabIndex = 19;
            this.labelMatch.Text = "Match";
            this.labelMatch.Click += new System.EventHandler(this.labelMatch_Click);
            // 
            // labelCountry
            // 
            this.labelCountry.AutoSize = true;
            this.labelCountry.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCountry.ForeColor = System.Drawing.Color.Navy;
            this.labelCountry.Location = new System.Drawing.Point(6, 111);
            this.labelCountry.Name = "labelCountry";
            this.labelCountry.Size = new System.Drawing.Size(75, 22);
            this.labelCountry.TabIndex = 18;
            this.labelCountry.Text = "Country";
            // 
            // labelFootballClub
            // 
            this.labelFootballClub.AutoSize = true;
            this.labelFootballClub.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelFootballClub.ForeColor = System.Drawing.Color.Navy;
            this.labelFootballClub.Location = new System.Drawing.Point(6, 73);
            this.labelFootballClub.Name = "labelFootballClub";
            this.labelFootballClub.Size = new System.Drawing.Size(122, 22);
            this.labelFootballClub.TabIndex = 17;
            this.labelFootballClub.Text = "Football Club";
            // 
            // labelNameTeam
            // 
            this.labelNameTeam.AutoSize = true;
            this.labelNameTeam.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNameTeam.ForeColor = System.Drawing.Color.Navy;
            this.labelNameTeam.Location = new System.Drawing.Point(6, 32);
            this.labelNameTeam.Name = "labelNameTeam";
            this.labelNameTeam.Size = new System.Drawing.Size(112, 22);
            this.labelNameTeam.TabIndex = 16;
            this.labelNameTeam.Text = "Name Team";
            // 
            // textBoxFootballClub
            // 
            this.textBoxFootballClub.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxFootballClub.ForeColor = System.Drawing.Color.Black;
            this.textBoxFootballClub.Location = new System.Drawing.Point(134, 71);
            this.textBoxFootballClub.Name = "textBoxFootballClub";
            this.textBoxFootballClub.Size = new System.Drawing.Size(123, 29);
            this.textBoxFootballClub.TabIndex = 12;
            // 
            // textBoxNameTeam
            // 
            this.textBoxNameTeam.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxNameTeam.ForeColor = System.Drawing.Color.Black;
            this.textBoxNameTeam.Location = new System.Drawing.Point(134, 31);
            this.textBoxNameTeam.Name = "textBoxNameTeam";
            this.textBoxNameTeam.Size = new System.Drawing.Size(123, 29);
            this.textBoxNameTeam.TabIndex = 11;
            // 
            // buttonNewTeam
            // 
            this.buttonNewTeam.BackColor = System.Drawing.Color.White;
            this.buttonNewTeam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNewTeam.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonNewTeam.ForeColor = System.Drawing.Color.Navy;
            this.buttonNewTeam.Location = new System.Drawing.Point(629, 18);
            this.buttonNewTeam.Name = "buttonNewTeam";
            this.buttonNewTeam.Size = new System.Drawing.Size(178, 41);
            this.buttonNewTeam.TabIndex = 4;
            this.buttonNewTeam.Text = "Create new team";
            this.buttonNewTeam.UseVisualStyleBackColor = false;
            this.buttonNewTeam.Click += new System.EventHandler(this.buttonNewTeam_Click);
            // 
            // groupBoxEditTeam
            // 
            this.groupBoxEditTeam.BackColor = System.Drawing.Color.FloralWhite;
            this.groupBoxEditTeam.Controls.Add(this.EdMatch);
            this.groupBoxEditTeam.Controls.Add(this.EdCountry);
            this.groupBoxEditTeam.Controls.Add(this.buttonSaveTeam);
            this.groupBoxEditTeam.Controls.Add(this.label_Match);
            this.groupBoxEditTeam.Controls.Add(this.label_Country);
            this.groupBoxEditTeam.Controls.Add(this.labelFootball_club);
            this.groupBoxEditTeam.Controls.Add(this.labelName_team);
            this.groupBoxEditTeam.Controls.Add(this.EdFootballClub);
            this.groupBoxEditTeam.Controls.Add(this.EdNameTeam);
            this.groupBoxEditTeam.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxEditTeam.Location = new System.Drawing.Point(233, 62);
            this.groupBoxEditTeam.Name = "groupBoxEditTeam";
            this.groupBoxEditTeam.Size = new System.Drawing.Size(272, 350);
            this.groupBoxEditTeam.TabIndex = 3;
            this.groupBoxEditTeam.TabStop = false;
            this.groupBoxEditTeam.Text = "Selected Team";
            // 
            // EdMatch
            // 
            this.EdMatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EdMatch.ForeColor = System.Drawing.Color.Black;
            this.EdMatch.FormattingEnabled = true;
            this.EdMatch.ItemHeight = 20;
            this.EdMatch.Location = new System.Drawing.Point(82, 154);
            this.EdMatch.Name = "EdMatch";
            this.EdMatch.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.EdMatch.Size = new System.Drawing.Size(184, 124);
            this.EdMatch.TabIndex = 23;
            // 
            // EdCountry
            // 
            this.EdCountry.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EdCountry.ForeColor = System.Drawing.Color.Black;
            this.EdCountry.Location = new System.Drawing.Point(122, 110);
            this.EdCountry.Name = "EdCountry";
            this.EdCountry.Size = new System.Drawing.Size(135, 29);
            this.EdCountry.TabIndex = 22;
            // 
            // buttonSaveTeam
            // 
            this.buttonSaveTeam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSaveTeam.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSaveTeam.ForeColor = System.Drawing.Color.Navy;
            this.buttonSaveTeam.Location = new System.Drawing.Point(22, 289);
            this.buttonSaveTeam.Name = "buttonSaveTeam";
            this.buttonSaveTeam.Size = new System.Drawing.Size(219, 43);
            this.buttonSaveTeam.TabIndex = 21;
            this.buttonSaveTeam.Text = "Save";
            this.buttonSaveTeam.UseVisualStyleBackColor = true;
            this.buttonSaveTeam.Click += new System.EventHandler(this.buttonSaveTeam_Click);
            // 
            // label_Match
            // 
            this.label_Match.AutoSize = true;
            this.label_Match.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Match.ForeColor = System.Drawing.Color.Navy;
            this.label_Match.Location = new System.Drawing.Point(7, 154);
            this.label_Match.Name = "label_Match";
            this.label_Match.Size = new System.Drawing.Size(60, 22);
            this.label_Match.TabIndex = 19;
            this.label_Match.Text = "Match";
            // 
            // label_Country
            // 
            this.label_Country.AutoSize = true;
            this.label_Country.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Country.ForeColor = System.Drawing.Color.Navy;
            this.label_Country.Location = new System.Drawing.Point(6, 109);
            this.label_Country.Name = "label_Country";
            this.label_Country.Size = new System.Drawing.Size(75, 22);
            this.label_Country.TabIndex = 18;
            this.label_Country.Text = "Country";
            // 
            // labelFootball_club
            // 
            this.labelFootball_club.AutoSize = true;
            this.labelFootball_club.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelFootball_club.ForeColor = System.Drawing.Color.Navy;
            this.labelFootball_club.Location = new System.Drawing.Point(6, 72);
            this.labelFootball_club.Name = "labelFootball_club";
            this.labelFootball_club.Size = new System.Drawing.Size(118, 22);
            this.labelFootball_club.TabIndex = 17;
            this.labelFootball_club.Text = "Football club";
            // 
            // labelName_team
            // 
            this.labelName_team.AutoSize = true;
            this.labelName_team.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelName_team.ForeColor = System.Drawing.Color.Navy;
            this.labelName_team.Location = new System.Drawing.Point(6, 32);
            this.labelName_team.Name = "labelName_team";
            this.labelName_team.Size = new System.Drawing.Size(112, 22);
            this.labelName_team.TabIndex = 16;
            this.labelName_team.Text = "Name Team";
            // 
            // EdFootballClub
            // 
            this.EdFootballClub.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EdFootballClub.ForeColor = System.Drawing.Color.Black;
            this.EdFootballClub.Location = new System.Drawing.Point(122, 72);
            this.EdFootballClub.Name = "EdFootballClub";
            this.EdFootballClub.Size = new System.Drawing.Size(135, 29);
            this.EdFootballClub.TabIndex = 12;
            // 
            // EdNameTeam
            // 
            this.EdNameTeam.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EdNameTeam.ForeColor = System.Drawing.Color.Black;
            this.EdNameTeam.Location = new System.Drawing.Point(125, 31);
            this.EdNameTeam.Name = "EdNameTeam";
            this.EdNameTeam.Size = new System.Drawing.Size(135, 29);
            this.EdNameTeam.TabIndex = 11;
            // 
            // buttonRemoveTeam
            // 
            this.buttonRemoveTeam.BackColor = System.Drawing.Color.White;
            this.buttonRemoveTeam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRemoveTeam.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonRemoveTeam.ForeColor = System.Drawing.Color.Navy;
            this.buttonRemoveTeam.Location = new System.Drawing.Point(288, 14);
            this.buttonRemoveTeam.Name = "buttonRemoveTeam";
            this.buttonRemoveTeam.Size = new System.Drawing.Size(186, 42);
            this.buttonRemoveTeam.TabIndex = 1;
            this.buttonRemoveTeam.Text = "Remove";
            this.buttonRemoveTeam.UseVisualStyleBackColor = false;
            this.buttonRemoveTeam.Click += new System.EventHandler(this.buttonRemoveTeam_Click);
            this.buttonRemoveTeam.MouseClick += new System.Windows.Forms.MouseEventHandler(this.buttonRemoveTeam_MouseClick);
            // 
            // listBoxTeam
            // 
            this.listBoxTeam.BackColor = System.Drawing.Color.FloralWhite;
            this.listBoxTeam.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBoxTeam.ForeColor = System.Drawing.Color.Navy;
            this.listBoxTeam.FormattingEnabled = true;
            this.listBoxTeam.ItemHeight = 20;
            this.listBoxTeam.Location = new System.Drawing.Point(9, 28);
            this.listBoxTeam.Name = "listBoxTeam";
            this.listBoxTeam.Size = new System.Drawing.Size(210, 384);
            this.listBoxTeam.TabIndex = 0;
            this.listBoxTeam.SelectedValueChanged += new System.EventHandler(this.listBoxTeam_SelectedValueChanged);
            // 
            // SearchTeam
            // 
            this.SearchTeam.Location = new System.Drawing.Point(16, 28);
            this.SearchTeam.Name = "SearchTeam";
            this.SearchTeam.Size = new System.Drawing.Size(284, 26);
            this.SearchTeam.TabIndex = 4;
            this.SearchTeam.TextChanged += new System.EventHandler(this.SearchTeam_TextChanged);
            // 
            // buttonSearchTeam
            // 
            this.buttonSearchTeam.BackColor = System.Drawing.Color.LightBlue;
            this.buttonSearchTeam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearchTeam.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSearchTeam.ForeColor = System.Drawing.Color.Navy;
            this.buttonSearchTeam.Location = new System.Drawing.Point(324, 18);
            this.buttonSearchTeam.Name = "buttonSearchTeam";
            this.buttonSearchTeam.Size = new System.Drawing.Size(131, 49);
            this.buttonSearchTeam.TabIndex = 5;
            this.buttonSearchTeam.Text = "Search";
            this.buttonSearchTeam.UseVisualStyleBackColor = false;
            this.buttonSearchTeam.Click += new System.EventHandler(this.buttonSearchTeam_Click);
            // 
            // TeamList
            // 
            this.TeamList.BackgroundColor = System.Drawing.Color.Azure;
            this.TeamList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TeamList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TeamName,
            this.ClubFootball,
            this.Country,
            this.IdMatch});
            this.TeamList.Location = new System.Drawing.Point(16, 73);
            this.TeamList.Name = "TeamList";
            this.TeamList.ReadOnly = true;
            this.TeamList.Size = new System.Drawing.Size(439, 327);
            this.TeamList.TabIndex = 0;
            this.TeamList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TeamList_CellContentClick);
            // 
            // TeamName
            // 
            this.TeamName.HeaderText = "Name Team";
            this.TeamName.Name = "TeamName";
            this.TeamName.ReadOnly = true;
            // 
            // ClubFootball
            // 
            this.ClubFootball.HeaderText = "Football Club";
            this.ClubFootball.Name = "ClubFootball";
            this.ClubFootball.ReadOnly = true;
            // 
            // Country
            // 
            this.Country.HeaderText = "Country";
            this.Country.Name = "Country";
            this.Country.ReadOnly = true;
            // 
            // IdMatch
            // 
            this.IdMatch.HeaderText = "Match";
            this.IdMatch.Name = "IdMatch";
            this.IdMatch.ReadOnly = true;
            // 
            // Informations
            // 
            this.Informations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Informations.Controls.Add(this.Players);
            this.Informations.Controls.Add(this.Team);
            this.Informations.Controls.Add(this.Match);
            this.Informations.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Informations.Location = new System.Drawing.Point(12, 8);
            this.Informations.Name = "Informations";
            this.Informations.SelectedIndex = 0;
            this.Informations.Size = new System.Drawing.Size(945, 529);
            this.Informations.TabIndex = 0;
            // 
            // CourseWork
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::InformationsSystem.Properties.Resources._1497784516_d091d0b5d0b7_d0bdd0b0d0b7d0b2d0b0d0bdd0b8d18f;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(969, 538);
            this.Controls.Add(this.Informations);
            this.Name = "CourseWork";
            this.Text = "CourseWork";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CourseWork_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Players.ResumeLayout(false);
            this.Players.PerformLayout();
            this.groupBoxAdminPlayer.ResumeLayout(false);
            this.AddPlayers.ResumeLayout(false);
            this.AddPlayers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AddCountMatch)).EndInit();
            this.groupBoxPlayer.ResumeLayout(false);
            this.groupBoxPlayer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EditCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayersList)).EndInit();
            this.Match.ResumeLayout(false);
            this.Match.PerformLayout();
            this.groupBoxAdminMatch.ResumeLayout(false);
            this.groupBoxSelectedMatch.ResumeLayout(false);
            this.groupBoxSelectedMatch.PerformLayout();
            this.AddMatch.ResumeLayout(false);
            this.AddMatch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MatchList)).EndInit();
            this.Team.ResumeLayout(false);
            this.Team.PerformLayout();
            this.groupBoxTeams.ResumeLayout(false);
            this.groupBoxAddTeam.ResumeLayout(false);
            this.groupBoxAddTeam.PerformLayout();
            this.groupBoxEditTeam.ResumeLayout(false);
            this.groupBoxEditTeam.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TeamList)).EndInit();
            this.Informations.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabPage Players;
        private System.Windows.Forms.TabPage Match;
        private System.Windows.Forms.GroupBox AddMatch;
        private System.Windows.Forms.TextBox textScore;
        private System.Windows.Forms.Button Add_Match;
        private System.Windows.Forms.Label labelScore;
        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.Label labelNameEvent;
        private System.Windows.Forms.TextBox textLocation;
        private System.Windows.Forms.TextBox textNameEvent;
        private System.Windows.Forms.Button buttonAdd_Match;
        private System.Windows.Forms.TextBox SearchMatch;
        private System.Windows.Forms.Button buttonSearchMatch;
        private System.Windows.Forms.DataGridView MatchList;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameEvent;
        private System.Windows.Forms.DataGridViewTextBoxColumn Location;
        private System.Windows.Forms.DataGridViewTextBoxColumn Score;
        private System.Windows.Forms.TabPage Team;
        private System.Windows.Forms.GroupBox groupBoxTeams;
        private System.Windows.Forms.GroupBox groupBoxAddTeam;
        private System.Windows.Forms.ListBox comboBoxMatch;
        private System.Windows.Forms.TextBox textBoxCountry;
        private System.Windows.Forms.Button buttonAddTeam;
        private System.Windows.Forms.Label labelMatch;
        private System.Windows.Forms.Label labelCountry;
        private System.Windows.Forms.Label labelFootballClub;
        private System.Windows.Forms.Label labelNameTeam;
        private System.Windows.Forms.TextBox textBoxFootballClub;
        private System.Windows.Forms.TextBox textBoxNameTeam;
        private System.Windows.Forms.Button buttonNewTeam;
        private System.Windows.Forms.GroupBox groupBoxEditTeam;
        private System.Windows.Forms.ListBox EdMatch;
        private System.Windows.Forms.TextBox EdCountry;
        private System.Windows.Forms.Button buttonSaveTeam;
        private System.Windows.Forms.Label label_Match;
        private System.Windows.Forms.Label label_Country;
        private System.Windows.Forms.Label labelFootball_club;
        private System.Windows.Forms.Label labelName_team;
        private System.Windows.Forms.TextBox EdFootballClub;
        private System.Windows.Forms.TextBox EdNameTeam;
        private System.Windows.Forms.Button buttonRemoveTeam;
        private System.Windows.Forms.ListBox listBoxTeam;
        private System.Windows.Forms.TextBox SearchTeam;
        private System.Windows.Forms.Button buttonSearchTeam;
        private System.Windows.Forms.DataGridView TeamList;
        private System.Windows.Forms.DataGridViewTextBoxColumn TeamName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClubFootball;
        private System.Windows.Forms.DataGridViewTextBoxColumn Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdMatch;
        private System.Windows.Forms.TextBox SearchPlayers;
        private System.Windows.Forms.DataGridView PlayersList;
        private System.Windows.Forms.DataGridViewTextBoxColumn NamePlayer;
        private System.Windows.Forms.DataGridViewTextBoxColumn Surname;
        private System.Windows.Forms.DataGridViewTextBoxColumn Birthday;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameTeam;
        private System.Windows.Forms.DataGridViewTextBoxColumn CountMatch;
        private System.Windows.Forms.GroupBox groupBoxAdminPlayer;
        private System.Windows.Forms.GroupBox AddPlayers;
        private System.Windows.Forms.Button buttonAddPlayers;
        private System.Windows.Forms.Label labelAddCountMatch;
        private System.Windows.Forms.Label labelAddTeam;
        private System.Windows.Forms.Label labelAddBirthday;
        private System.Windows.Forms.Label labelAddSurname;
        private System.Windows.Forms.Label labelAddName;
        private System.Windows.Forms.NumericUpDown AddCountMatch;
        private System.Windows.Forms.ComboBox AddTeam;
        private System.Windows.Forms.DateTimePicker AddBirthday;
        private System.Windows.Forms.TextBox AddSurname;
        private System.Windows.Forms.TextBox AddName;
        private System.Windows.Forms.Button AddPlayer;
        private System.Windows.Forms.GroupBox groupBoxPlayer;
        private System.Windows.Forms.Button buttonEditSave;
        private System.Windows.Forms.Label labelEditCountMatch;
        private System.Windows.Forms.Label labelEditTeam;
        private System.Windows.Forms.Label labelEditBirthday;
        private System.Windows.Forms.Label labelEditSurname;
        private System.Windows.Forms.Label labelEditName;
        private System.Windows.Forms.NumericUpDown EditCount;
        private System.Windows.Forms.ComboBox EditTeam;
        private System.Windows.Forms.DateTimePicker EditBirthday;
        private System.Windows.Forms.TextBox EditSurname;
        private System.Windows.Forms.TextBox EditName;
        private System.Windows.Forms.Button RemovePlayer;
        private System.Windows.Forms.ListBox listBoxPlayers;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.TabControl Informations;
        private System.Windows.Forms.Button buttonMatchDelete;
        private System.Windows.Forms.GroupBox groupBoxAdminMatch;
        private System.Windows.Forms.GroupBox groupBoxSelectedMatch;
        private System.Windows.Forms.TextBox textBox_Score;
        private System.Windows.Forms.Button buttonSaveMatch;
        private System.Windows.Forms.Label label_Score;
        private System.Windows.Forms.Label label_Location;
        private System.Windows.Forms.Label labelName_Event;
        private System.Windows.Forms.TextBox textBox_Location;
        private System.Windows.Forms.TextBox textBox_NameEvent;
        private System.Windows.Forms.ListBox listBoxMatch;
    }
}

