﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InformationsSystem {
    using System;
    
    
    /// <summary>
    ///   Класс ресурса со строгой типизацией для поиска локализованных строк и т.д.
    /// </summary>
    // Этот класс создан автоматически классом StronglyTypedResourceBuilder
    // с помощью такого средства, как ResGen или Visual Studio.
    // Чтобы добавить или удалить член, измените файл .ResX и снова запустите ResGen
    // с параметром /str или перестройте свой проект VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    partial class CourseWork {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CourseWork() {
        }
        
        /// <summary>
        ///   Возвращает кэшированный экземпляр ResourceManager, использованный этим классом.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("InformationsSystem.CourseWork", typeof(CourseWork).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Перезаписывает свойство CurrentUICulture текущего потока для всех
        ///   обращений к ресурсу с помощью этого класса ресурса со строгой типизацией.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Boolean, аналогичного True.
        /// </summary>
        public static bool Birthday_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("Birthday.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Boolean, аналогичного True.
        /// </summary>
        public static bool ClubFootball_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("ClubFootball.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Boolean, аналогичного True.
        /// </summary>
        public static bool CountMatch_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("CountMatch.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Boolean, аналогичного True.
        /// </summary>
        public static bool Country_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("Country.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Boolean, аналогичного True.
        /// </summary>
        public static bool IdMatch_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("IdMatch.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Boolean, аналогичного True.
        /// </summary>
        public static bool Location_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("Location.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap Match_BackgroundImage {
            get {
                object obj = ResourceManager.GetObject("Match.BackgroundImage", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Boolean, аналогичного True.
        /// </summary>
        public static bool NameEvent_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("NameEvent.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Boolean, аналогичного True.
        /// </summary>
        public static bool NamePlayer_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("NamePlayer.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Boolean, аналогичного True.
        /// </summary>
        public static bool NameTeam_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("NameTeam.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Boolean, аналогичного True.
        /// </summary>
        public static bool Score_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("Score.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Boolean, аналогичного True.
        /// </summary>
        public static bool Surname_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("Surname.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Поиск локализованного ресурса типа System.Boolean, аналогичного True.
        /// </summary>
        public static bool TeamName_UserAddedColumn {
            get {
                object obj = ResourceManager.GetObject("TeamName.UserAddedColumn", resourceCulture);
                return ((bool)(obj));
            }
        }
    }
}
