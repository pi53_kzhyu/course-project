﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Football;
using System.IO;
using System.Globalization;



namespace InformationsSystem
{
    public partial class CourseWork : Form
    {
        public Players EdPlayers;
        bool isAdmin;
        Players[] players;
        Dictionary<string, string> listPlayers = new Dictionary<string, string>();
        Dictionary<uint, string> listTeams = new Dictionary<uint, string>();
        Dictionary<uint, string> listMatch = new Dictionary<uint, string>();
        Team[] teams;
        Match[] matchs;
        WorkFile workFile;
        public CourseWork(bool _isAdmin)
        {
            isAdmin = _isAdmin;
            InitializeComponent();
        }
        private void UpdateListPlayers()
        {
            listBoxPlayers.DataSource = new BindingSource(listPlayers, null);
            listBoxPlayers.DisplayMember = "Value";
            listBoxPlayers.ValueMember = "Key";
        }
        private void UpdateListTeams()
        {
            listBoxTeam.DataSource = new BindingSource(listTeams, null);
            listBoxTeam.DisplayMember = "Value";
            listBoxTeam.ValueMember = "Key";

        }
        private void UpdateListMatchs()
        {
            listBoxMatch.DataSource = new BindingSource(listMatch, null);
            listBoxMatch.DisplayMember = "Value";
            listBoxMatch.ValueMember = "Key";

        }
        private string GetTeamById(uint id)
        {
            string nameTeam = "";
            foreach (Team team in teams)
            {
                if (team.GetId() == id)
                {
                    nameTeam = team.GetNameTeam();
                    break;
                }
            }
            return nameTeam;
        }
        private string GetMatchById(uint id)
        {
            string nameMatch = "";
            foreach (Match match in matchs)
            {
                if (match.GetId() == id)
                {
                    nameMatch = match.GetEventName();
                    break;
                }
            }
            return nameMatch;
        }
        private void UpdateListTeam()
        {
            EditTeam.DataSource = new BindingSource(listTeams, null);
            EditTeam.DisplayMember = "Value";
            EditTeam.ValueMember = "Key";

            AddTeam.DataSource = new BindingSource(listTeams, null);
            AddTeam.DisplayMember = "Value";
            AddTeam.ValueMember = "Key";
        }
        private void UpdateListMatch()
        {
            comboBoxMatch.DataSource = new BindingSource(listMatch, null);
            comboBoxMatch.DisplayMember = "Value";
            comboBoxMatch.ValueMember = "Key";

            EdMatch.DataSource = new BindingSource(listMatch, null);
            EdMatch.DisplayMember = "Value";
            EdMatch.ValueMember = "Key";
        }
        private void Form1_Load(object sender, EventArgs e)
        {


            workFile = new WorkFile();
            //Get Match
            string[,] matchFile = workFile.getFileArray(@"D:\Курсова робота\Match.txt");
            matchs = new Match[matchFile.GetLength(0)];
            for (int index = 0; index < matchFile.GetLength(0); index++)
                matchs[index] = new Match(Convert.ToUInt32(matchFile[index, 0]), matchFile[index, 1], matchFile[index, 2], matchFile[index, 3]);

            foreach (Match match in matchs)
            {
                PrintMatchData(match);
                listMatch.Add(match.GetId(), match.GetEventName());
            }
            UpdateListMatchs();
            //Get Team
            string[,] teamFile = workFile.getFileArray(@"D:\Курсова робота\Team.txt");
            teams = new Team[teamFile.GetLength(0)];
            for (int index = 0; index < teamFile.GetLength(0); index++)
                teams[index] = new Team(Convert.ToUInt32(teamFile[index, 0]), teamFile[index, 1], teamFile[index, 2], teamFile[index, 3], teamFile[index, 4]);

            foreach (Team team in teams)
            {
                PrintTeamData(team);
                listTeams.Add(team.GetId(), team.GetNameTeam());
            }
            UpdateListTeams();
            if (isAdmin)
            {
                groupBoxTeams.Visible = true;
                TeamList.Visible = false;
                Team.Hide();
                UpdateListMatch();
                groupBoxAddTeam.Visible = false;
                SearchTeam.Visible = false;
                buttonSearchTeam.Visible = false;

                groupBoxAdminPlayer.Visible = true;
                PlayersList.Visible = false;

                UpdateListTeam();
                AddPlayers.Visible = true;
                SearchPlayers.Visible = false;
                buttonSearch.Visible = false;
                AddMatch.Visible = false;
                SearchMatch.Hide();
                buttonSearchMatch.Hide();
                AddPlayers.Hide();
                MatchList.Hide();
                groupBoxAdminMatch.Visible = true;



            }
            else
            {

                groupBoxAdminPlayer.Visible = false;
                PlayersList.Visible = true;
                groupBoxTeams.Visible = false;
                TeamList.Visible = true;
                AddMatch.Visible = false;
                buttonAdd_Match.Visible = false;
                MatchList.Visible = true;
                groupBoxAdminMatch.Visible = false;

            }
            //Get Players
            string[,] playersArray = workFile.getFileArray(@"D:\Курсова робота\Players.txt");
            PlayersList pl = new PlayersList();
            players = pl.GenerationPlayersList(playersArray.GetLength(0), playersArray);
            foreach (Players player in players)
            {
                PrintPlayerData(player);
                listPlayers.Add(player.GetId().ToString(), player.GetName() + ' ' + player.GetSurname());

            }
            UpdateListPlayers();
        }
        private List<string> getPlayersArray()
        {
            List<string> _playersArray = new List<string>();
            for (int i = 0; i < players.Length; i++)
            {
                if (players[i] != null)
                {
                    _playersArray.Add(players[i].GetId().ToString() + " " + players[i].GetName() + " " + players[i].GetSurname() + " " + players[i].GetBirthday().ToString("dd.MM.yyyy") + " " + players[i].GetTeam() + " " + players[i].GetCountMatch());
                }
            }

            return _playersArray;
        }
        private List<string> getTeamArray()
        {
            List<string> _teamsArray = new List<string>();
            for (int i = 0; i < teams.Length; i++)
            {
                if (teams[i] != null)
                {
                    string _matches = "";
                    foreach (string item in teams[i].GetIdsMatchs())
                    {
                        if (_matches != "")
                        {
                            _matches += ',';
                        }
                        _matches += item;
                    }
                    _teamsArray.Add(teams[i].GetId().ToString() + " " + teams[i].GetNameTeam() + " " + teams[i].GetFootballClub() + " " + teams[i].GetCountry() + " " + _matches);
                }
            }
            return _teamsArray;
        }
        private List<string> getMatchArray()
        {
            List<string> _matchArray = new List<string>();
            for (int i = 0; i < matchs.Length; i++)
            {
                if (matchs[i] != null)

                    _matchArray.Add(matchs[i].GetId().ToString() + " " + matchs[i].GetEventName() + " " + matchs[i].GetLocation() + " " + matchs[i].GetScore());

            }
            return _matchArray;
        }

        private void CourseWork_FormClosed(object sender, FormClosedEventArgs e)
        {

            workFile.WriteFileData(@"D:\Курсова робота\Players.txt", getPlayersArray());
            workFile.WriteFileData(@"D:\Курсова робота\Team.txt", getTeamArray());
            workFile.WriteFileData(@"D:\Курсова робота\Match.txt", getMatchArray());
            Application.Exit();
        }

        private void RemovePlayer_MouseClick(object sender, MouseEventArgs e)
        {
            uint selectedIndex = Convert.ToUInt32(listBoxPlayers.SelectedIndex);
            string selectedItem = listBoxPlayers.SelectedItem.ToString();
            string selectedId = listBoxPlayers.SelectedValue.ToString();
            DialogResult dialogResult = MessageBox.Show("Do you want delete " + selectedItem + " players?", "Delete", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                players[selectedIndex] = null;
                listPlayers.Remove(selectedId);
                UpdateListPlayers();
            }

        }


        private void listBoxPlayers_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBoxPlayers_SelectedValueChanged(object sender, EventArgs e)
        {
            string selectedId = "";
            if (listBoxPlayers.SelectedValue != null)
            {
                selectedId = listBoxPlayers.SelectedValue.ToString();
            }
            if (listBoxPlayers.SelectedItem != null)
            {

                foreach (Players player in players)
                {
                    if (player != null && player.GetId().ToString() == selectedId)
                    {
                        EditName.Text = player.GetName();
                        EditSurname.Text = player.GetSurname();
                        EditBirthday.Text = player.GetBirthday().ToString();
                        EditTeam.SelectedValue = player.GetTeam();
                        EditCount.Text = player.GetCountMatch().ToString();
                    }
                }

            }
        }

        private void buttonEditSave_Click(object sender, EventArgs e)
        {
            string selectedId = "";
            if (listBoxPlayers.SelectedValue != null)
            {
                selectedId = listBoxPlayers.SelectedValue.ToString();
            }
            listPlayers = new Dictionary<string, string>();
            foreach (Players player in players)
            {
                if (player.GetId().ToString() == selectedId)
                {
                    player.SetName(EditName.Text);
                    player.SetSurname(EditSurname.Text);
                    player.SetBirthday(Convert.ToDateTime(EditBirthday.Text));
                    player.SetTeam(Convert.ToUInt32(EditTeam.SelectedValue));
                    player.SetCountMatch(Convert.ToUInt32(EditCount.Text));

                }
                listPlayers.Add(player.GetId().ToString(), player.GetName() + ' ' + player.GetSurname());
            }
            UpdateListPlayers();


        }

        private void AddPlayer_Click(object sender, EventArgs e)
        {
            AddPlayers.Visible = true;
        }
        private uint GenerationRandomId(uint _id)
        {
            uint id = _id + 1;

            return id;
        }
        private void AddClear()
        {
            AddName.Clear();
            AddSurname.Clear();
            AddBirthday.Text = "";
            AddTeam.Text = "";
            AddCountMatch.Text = "";
        }
        private void AddTeamClear()
        {
            textBoxNameTeam.Clear();
            textBoxFootballClub.Clear();
            textBoxCountry.Clear();
            comboBoxMatch.ClearSelected();

        }
        private void AddMatchClear()
        {
            textNameEvent.Clear();
            textLocation.Clear();
            textScore.Clear();
        }
        private void buttonAddPlayers_Click(object sender, EventArgs e)
        {
            int index = players.Length;
            uint lastId = 0;
            if (AddName.Text != "" && AddSurname.Text != "" && AddTeam.Text != "")
            {
                Players[] tempPlayers = new Players[players.Length + 1];
                for (int i = 0; i < players.Length; i++)
                {
                    tempPlayers[i] = players[i];
                    if (players[i] != null)
                        lastId = players[i].GetId();
                }
                tempPlayers[index] = new Players(GenerationRandomId(lastId), AddSurname.Text, AddName.Text, DateTime.Parse(AddBirthday.Text), Convert.ToUInt32(AddTeam.SelectedValue), Convert.ToUInt32(AddCountMatch.Text));
                players = tempPlayers;

                listPlayers.Add(players[index].GetId().ToString(), players[index].GetName() + ' ' + players[index].GetSurname());
                UpdateListPlayers();
                AddClear();
            }
            else
                MessageBox.Show("Please input correct data!");

        }

        private void AddPlayers_Enter(object sender, EventArgs e)
        {

        }
        private void PrintPlayerData(Players _player)
        {
            PlayersList.Rows.Add(_player.GetName(), _player.GetSurname(), _player.GetBirthday().ToString("dd.MM.yyyy"), GetTeamById(_player.GetTeam()), _player.GetCountMatch());
        }
        private void PrintTeamData(Team _team)
        {
            string matchNames = "";
            string[] matchesIds = _team.GetIdsMatchs();
            foreach (string id in matchesIds)
            {
                matchNames += GetMatchById(Convert.ToUInt32(id)) + ";";
            }
            TeamList.Rows.Add(_team.GetNameTeam(), _team.GetFootballClub(), _team.GetCountry(), matchNames);
        }
        private void PrintMatchData(Match _match)
        {
            MatchList.Rows.Add(_match.GetEventName(), _match.GetLocation(), _match.GetScore());

        }
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            string searchValue = SearchPlayers.Text;
            string[] searchArray = searchValue.Split(' ');
            PlayersList.RowCount = 1;
            foreach (Players player in players)
            {
                if (searchArray.Length > 1)
                {
                    if (player.GetName().ToLower().Contains(searchArray[0].ToLower()) && player.GetSurname().ToLower().Contains(searchArray[1].ToLower()))
                        PrintPlayerData(player);
                }
                else
                {
                    if (player.GetName().ToLower().Contains(searchValue.ToLower()))
                        PrintPlayerData(player);
                }

            }

        }

        private void buttonSearchTeam_Click(object sender, EventArgs e)
        {
            string searchValue = SearchTeam.Text;
            TeamList.RowCount = 1;
            foreach (Team team in teams)
            {
                if (team.GetNameTeam().ToLower().Contains(searchValue.ToLower()))
                    PrintTeamData(team);
            }
        }

        private void buttonSearchMatch_Click(object sender, EventArgs e)
        {
            string searchValue = SearchMatch.Text;
            MatchList.RowCount = 1;
            foreach (Match match in matchs)
            {
                if (match.GetEventName().ToLower().Contains(searchValue.ToLower()))
                    PrintMatchData(match);
            }

        }

        private void TeamList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            groupBoxAddTeam.Visible = true;

        }

        private void RemovePlayer_Click(object sender, EventArgs e)
        {

        }

        private void buttonRemoveTeam_MouseClick(object sender, MouseEventArgs e)
        {
            uint selectedIndex = Convert.ToUInt16(listBoxTeam.SelectedIndex);
            string selectedItem = listBoxTeam.SelectedItem.ToString();
            string selectedId = listBoxTeam.SelectedValue.ToString();

            DialogResult dialogResult = MessageBox.Show("Do you want delete " + selectedItem + " players?", "Delete", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                teams[selectedIndex] = null;
                listTeams.Remove(Convert.ToUInt32(selectedId));
                UpdateListTeams();
                UpdateListTeam();
                foreach (Players player in players)
                {
                    if (player.GetTeam() == Convert.ToUInt32(selectedId))
                        player.SetTeam(1);
                }
            }
        }

        private void buttonNewTeam_Click(object sender, EventArgs e)
        {
            groupBoxAddTeam.Visible = true;
        }

        private void buttonAddTeam_Click_1(object sender, EventArgs e)
        {
            int index = teams.Length;
            uint lastId = 0;
            string _matcheIds = "";
            if (textBoxNameTeam.Text != "" && textBoxFootballClub.Text != "" && textBoxCountry.Text != "")
            {
                Team[] tempTeams = new Team[teams.Length + 1];
                for (int i = 0; i < teams.Length; i++)
                {
                    tempTeams[i] = teams[i];
                    if (teams[i] != null)
                        lastId = teams[i].GetId();
                }
                foreach (var item in comboBoxMatch.SelectedItems)
                {
                    if (_matcheIds != "")
                    {
                        _matcheIds += ",";
                    }
                    _matcheIds += item.GetType().GetProperty("Key").GetValue(item, null).ToString();
                }
                tempTeams[index] = new Team(GenerationRandomId(lastId), textBoxNameTeam.Text, textBoxFootballClub.Text, textBoxCountry.Text, _matcheIds);
                teams = tempTeams;

                listTeams.Add(teams[index].GetId(), teams[index].GetNameTeam());
                UpdateListTeams();
                AddTeamClear();


            }
            else
                MessageBox.Show("Please input correct data!");
        }
        private void listBoxTeam_SelectedValueChanged(object sender, EventArgs e)
        {
            string selectedId = " ";
            string[] _matches;
            if (listBoxTeam.SelectedValue != null)
                selectedId = listBoxTeam.SelectedValue.ToString();

            if (listBoxTeam.SelectedItem != null)
            {
                foreach (Team team in teams)
                {

                    if (team != null && team.GetId().ToString() == selectedId)
                    {
                        EdNameTeam.Text = team.GetNameTeam();
                        EdFootballClub.Text = team.GetFootballClub();
                        EdCountry.Text = team.GetCountry();
                        _matches = team.GetIdsMatchs();
                        EdMatch.ClearSelected();
                        if (EdMatch.Items.Count > 0)
                        {
                            for (int i = 0; i < _matches.Length; i++)
                            {

                                for (int index = 0; index < matchs.Length; index++)
                                {
                                    if (_matches[i] == matchs[index].GetId().ToString())
                                    {
                                        EdMatch.SetSelected(index, true);
                                    }
                                }
                            }
                        }

                    }
                }
            }

        }
        private void buttonSaveTeam_Click(object sender, EventArgs e)
        {
            string selectedId = " ";
            if (listBoxTeam.SelectedValue != null)
            {
                selectedId = listBoxTeam.SelectedValue.ToString();
            }
            listTeams = new Dictionary<uint, string>();
            string _matcheIds = "";
            foreach (var item in EdMatch.SelectedItems)
            {
                if (_matcheIds != "")
                {
                    _matcheIds += ",";
                }
                _matcheIds += item.GetType().GetProperty("Key").GetValue(item, null).ToString();
            }
            foreach (Team team in teams)
            {
                if (team.GetId().ToString() == selectedId)
                {
                    team.SetNameTeam(EdNameTeam.Text);
                    team.SetFootballClub(EdFootballClub.Text);
                    team.SetCountry(EdCountry.Text);
                    team.SetIdMatch(Convert.ToString(_matcheIds));

                }
                listTeams.Add(team.GetId(), team.GetNameTeam());
            }
            UpdateListTeams();
        }

        private void groupBoxPlayer_Enter(object sender, EventArgs e)
        {

        }

        private void comboBoxMatch_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonRemoveTeam_Click(object sender, EventArgs e)
        {
            
        }

        private void buttonAdd_Match_Click(object sender, EventArgs e)
        {
            AddMatch.Visible = true;
        }

        private void Add_Match_Click(object sender, EventArgs e)
        {
            int index = matchs.Length;
            uint lastId = 0;
            if (textNameEvent.Text != "" && textLocation.Text != "" && textScore.Text != "")
            {
                Match[] match = new Match[matchs.Length + 1];
                for (int i = 0; i < matchs.Length; i++)
                {
                    match[i] = matchs[i];
                    if (matchs[i] != null)
                        lastId = matchs[i].GetId();
                }
                match[index] = new Match(GenerationRandomId(lastId), textNameEvent.Text, textLocation.Text, textScore.Text);
                matchs = match;
                listMatch.Add(matchs[index].GetId(), matchs[index].GetEventName());
                UpdateListMatchs();
                AddMatchClear();
            }
        }

        private void MatchList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void MatchList_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buttonMatchDelete_Click(object sender, EventArgs e)
        {
            uint selectedIndex = Convert.ToUInt32(listBoxMatch.SelectedIndex);
            string selectedItem = listBoxMatch.SelectedItem.ToString();
            string selectedId = listBoxMatch.SelectedValue.ToString();
            DialogResult dialogResult = MessageBox.Show("Do you want delete " + selectedItem + " players?", "Delete", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                matchs[selectedIndex] = null;
                listMatch.Remove(Convert.ToUInt32(selectedId));
                UpdateListMatchs();
            }
        }

        private void listBoxMatch_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBoxMatch_SelectedValueChanged(object sender, EventArgs e)
        {
            string selectedId = " ";

            if (listBoxMatch.SelectedValue != null)
                selectedId = listBoxMatch.SelectedValue.ToString();

            if (listBoxMatch.SelectedItem != null)
            {
                foreach (Match match in matchs)
                {

                    if (match != null && match.GetId().ToString() == selectedId)
                    {
                        textBox_NameEvent.Text = match.GetEventName();
                        textBox_Location.Text = match.GetLocation();
                        textBox_Score.Text = match.GetScore();
                    }
                }
            }
        }

        private void buttonMatchDelete_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void buttonSaveMatch_Click(object sender, EventArgs e)
        {
            string selectedId = "";
            if (listBoxMatch.SelectedValue != null)
            {
                selectedId = listBoxMatch.SelectedValue.ToString();
            }
            listMatch = new Dictionary<uint, string>();
            foreach (Match match in matchs)
            {
                if (match.GetId().ToString() == selectedId)
                {
                    match.SetEventName(textBox_NameEvent.Text);
                    match.SetLocation(textBox_Location.Text);
                    match.SetScore(textBox_Score.Text);

                }
                listMatch.Add(match.GetId(), match.GetEventName());
            }
            UpdateListMatchs();
        }

        private void labelMatch_Click(object sender, EventArgs e)
        {

        }

        private void SearchTeam_TextChanged(object sender, EventArgs e)
        {

        }

        private void AddMatch_Enter(object sender, EventArgs e)
        {

        }

        private void groupBoxTeams_Enter(object sender, EventArgs e)
        {

        }
    }
}
